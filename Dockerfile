FROM python:3.6.5-stretch

RUN mkdir -p /usr/src/app/tuto_docker_flask/
WORKDIR /usr/src/app/tuto_docker_flask
COPY app.py ./
# RUN useradd -ms /bin/bash mereen

# split dependencies install from application one to take advantage of layer caching
# since the dependecies will not change very often, which is not the case for the
# application

# be sure to work with the latest version of pip
# RUN pip install pip -U

COPY requirements.txt ./
RUN pip install -r requirements.txt

# stable layers
COPY docker-entrypoint.sh ./
RUN cat app.py
ENTRYPOINT ["python"]
CMD ["app.py"]
## unstable layers
#COPY setup.py ./
#COPY mereen/ mereen/
#RUN python setup.py install



